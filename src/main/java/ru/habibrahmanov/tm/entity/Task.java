package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
public final class Task extends AbstractEntity implements Comparable<Task> {
    private String projectId;

    public Task() {
    }

    public Task(String name, String id, String projectId, String userId) {
        this.name = name;
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
    }

    public Task(String id, String projectId, String userId, String name, String description, Date dateBegin, Date dateEnd) {
        this.id = id;
        this.projectId = projectId;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull Task o) {
        return this.getStatus().compareTo(o.getStatus());
    }
}
