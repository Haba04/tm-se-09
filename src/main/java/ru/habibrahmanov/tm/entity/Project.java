package ru.habibrahmanov.tm.entity;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.enumeration.Status;

import java.util.Date;

@Getter
@Setter
public final class Project extends AbstractEntity implements Comparable<Project>  {
    public Project() {
    }

    public Project(String id, String userId, String name, String description, Date dateBegin, Date dateEnd) {
        this.id = id;
        this.userId = userId;
        this.name = name;
        this.description = description;
        this.dateBegin = dateBegin;
        this.dateEnd = dateEnd;
    }

    @Override
    public int compareTo(@NotNull Project o) {
        return this.getStatus().compareTo(o.getStatus());
    }
}
