package ru.habibrahmanov.tm;

import ru.habibrahmanov.tm.bootstrap.Bootstrap;
import ru.habibrahmanov.tm.command.system.*;
import ru.habibrahmanov.tm.command.project.*;
import ru.habibrahmanov.tm.command.task.*;
import ru.habibrahmanov.tm.command.user.*;

public class Application {
    public static void main(String[] args) throws Exception {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
