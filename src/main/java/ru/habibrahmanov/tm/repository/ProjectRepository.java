package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.comporator.DateBeginComparator;
import ru.habibrahmanov.tm.comporator.DateEndComparator;
import ru.habibrahmanov.tm.entity.Project;
import java.util.*;


public final class ProjectRepository extends AbstractRepository<Project> implements IProjectRepository {
    @NotNull
    public Map<String, Project> getProjectMap() {
        return entities;
    }

    @Override
    public void persist(@NotNull final Project project) {
        entities.put(project.getId(), project);
    }

    @NotNull
    @Override
    public List<Project> findAll(@NotNull final String userId) {
        @NotNull final List<Project> projectList = new ArrayList<>();
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                projectList.add(project);
            }
        }
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateBegin(@NotNull final String userId) {
        @NotNull final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new DateBeginComparator<Project>());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByDateEnd(@NotNull final String userId) {
        @NotNull final List<Project> projectList = findAll(userId);
        Collections.sort(projectList, new DateEndComparator<Project>());
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> findAllSortedByStatus(@NotNull final String userId) {
        @Nullable final List<Project> projectList = findAll(userId);
        Collections.sort(projectList);
        return projectList;
    }

    @NotNull
    @Override
    public List<Project> searchByString (@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Project> projectList = findAll(userId);
        @NotNull final List<Project> projectListWithMatches = new ArrayList<>();
        for (int i = 0; i < projectList.size(); i++) {
            if (projectList.get(i).getName().contains(string) || projectList.get(i).getDescription().contains(string)) {
                projectListWithMatches.add(projectList.get(i));
            }
        }
        return projectListWithMatches;
    }

    @NotNull
    @Override
    public Project findOne(@NotNull final String projectId, @NotNull final String userId) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            return entities.get(projectId);
        }
        return null;
    }

    @Override
    public boolean removeAll(@NotNull final String userId) {
        for (Project project : entities.values()) {
            if (project.getUserId().equals(userId)) {
                entities.remove(project.getId());
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean remove(@NotNull final String projectId, @NotNull final String userId) {
        if (entities.get(projectId).getUserId().equals(userId)) {
            entities.remove(projectId);
            return true;
        }
        return false;
    }

    @Override
    public void merge(@NotNull final Project project) {
        if (entities.containsKey(project.getId())) {
            update(project.getUserId(), project.getId(), project.getName(), project.getDescription(), project.getDateBegin(), project.getDateEnd());
        }
        persist(project);
    }

    @Override
    public boolean update(
            @NotNull final String userId, @NotNull final String projectId, @NotNull final String name,
            @NotNull final String description, @NotNull final Date dateBegin, @NotNull final Date dateEnd
    ) {
        if (!entities.get(projectId).getUserId().equals(userId)) return false;
        entities.get(projectId).setName(name);
        entities.get(projectId).setDescription(description);
        entities.get(projectId).setDateBegin(dateBegin);
        entities.get(projectId).setDateEnd(dateEnd);
        return true;
    }
}

