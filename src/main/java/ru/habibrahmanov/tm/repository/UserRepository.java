package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.entity.User;
import java.util.*;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {
    @Override
    public void persist(@NotNull final User user) {
        entities.put(user.getId(), user);
    }

    @Nullable
    @Override
    public User findOne(@NotNull final String userId) {
        return entities.get(userId);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final List<User> userList = new ArrayList<>(entities.values());
        for (int i = 0; i < userList.size(); i++) {
            if (userList.get(i).getLogin().equals(login))
                return userList.get(i);
        }
        return null;
    }

    @NotNull
    @Override
    public List<User> findAll() {
        return new ArrayList<>(entities.values());
    }

    @Override
    public void removeOne(@NotNull final String login) {
        entities.remove(login);
    }

    @Override
    public void removeAll() {
        entities.clear();
    }

    @Override
    public void update(@NotNull final String userId, @NotNull final String login) {
        entities.get(userId).setLogin(login);
    }

    @Override
    public void merge(@NotNull final User user) {
        if (entities.containsKey(user.getId())) {
            update(user.getId(), user.getLogin());
        } else {
            persist(user);
        }
    }
}
