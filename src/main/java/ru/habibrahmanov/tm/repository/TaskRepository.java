package ru.habibrahmanov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.comporator.DateBeginComparator;
import ru.habibrahmanov.tm.comporator.DateEndComparator;
import ru.habibrahmanov.tm.entity.Task;

import java.util.*;

public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void persist(@NotNull final Task task) {
        entities.put(task.getId(), task);
    }

    @Override
    public Task findOne(@NotNull final String taskId, @NotNull final String userId) {
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                return entities.get(taskId);
            }
        }
        return null;
    }

    @Override
    public List<Task> findAll(@NotNull final String userId) {
        @NotNull final List<Task> taskList = new ArrayList<>();
        for (Task task : entities.values()){
            if (task.getUserId().equals(userId))
            taskList.add(task);
        }
        return taskList;
    }

    @Override
    public List<Task> findAllSortedByDateBegin(@NotNull final String userId) {
        @NotNull final List<Task> taskList = findAll(userId);
        Collections.sort(taskList, new DateBeginComparator<Task>());
        return taskList;
    }

    @Override
    public List<Task> findAllSortedByDateEnd(@NotNull final String userId) {
        @NotNull final List<Task> projectList = findAll(userId);
        Collections.sort(projectList, new DateEndComparator<Task>());
        return projectList;
    }

    @Override
    public List<Task> findAllSortedByStatus(@NotNull final String userId) {
        @NotNull final List<Task> taskList = findAll(userId);
        Collections.sort(taskList);
        return taskList;
    }

    @Override
    public List<Task> searchByString (@NotNull final String userId, @NotNull final String string) {
        @NotNull final List<Task> taskList = findAll(userId);
        @NotNull final List<Task> taskListWithMatches = new ArrayList<>();
        for (int i = 0; i < taskList.size(); i++) {
            if (taskList.get(i).getName().contains(string) || taskList.get(i).getDescription().contains(string)) {
                taskListWithMatches.add(taskList.get(i));
            }
        }
        return taskListWithMatches;
    }

    @Override
    public boolean remove(@NotNull final String userId, @NotNull final String taskId) {
        if (entities.get(taskId).getUserId().equals(userId)) {
            entities.remove(taskId);
            return true;
        }
        return false;
    }

    @Override
    public boolean removeAll(@NotNull final String userId, @NotNull final String projectId) {
        for (Task task : entities.values()) {
            if (task.getUserId().equals(userId)) {
                if (task.getProjectId().equals(projectId)) {
                    entities.remove(task.getId());
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean update(@NotNull final String userId, @NotNull final String id, @NotNull final String name, @NotNull final String description,
                       @NotNull final Date dateBegin, @NotNull final Date dateEnd) {
        if (!entities.get(id).getUserId().equals(userId)) return false;
        entities.get(id).setName(name);
        entities.get(id).setDescription(description);
        entities.get(id).setDateBegin(dateBegin);
        entities.get(id).setDateEnd(dateEnd);
        return true;
    }

    @Override
    public void merge(@NotNull final Task task) {
        if (entities.containsKey(task.getId())) {
            update(task.getUserId(), task.getId(), task.getName(), task.getDescription(), task.getDateBegin(), task.getDateEnd());
        } else {
            persist(task);
        }
    }
}

