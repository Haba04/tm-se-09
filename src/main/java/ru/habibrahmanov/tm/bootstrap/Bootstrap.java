package ru.habibrahmanov.tm.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.reflections.Reflections;
import ru.habibrahmanov.tm.api.*;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;
import ru.habibrahmanov.tm.repository.ProjectRepository;
import ru.habibrahmanov.tm.service.ProjectService;
import ru.habibrahmanov.tm.service.TaskService;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.service.UserService;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

public final class Bootstrap implements ServiceLocator {
    @NotNull private final IProjectRepository projectRepository = new ProjectRepository();
    @NotNull private final ITaskRepository taskRepository = new ru.habibrahmanov.tm.repository.TaskRepository();
    @NotNull private final IUserRepository userRepository = new ru.habibrahmanov.tm.repository.UserRepository();
    @NotNull private final IProjectService projectService = new ProjectService(projectRepository);
    @NotNull private final ITaskService taskService = new TaskService(taskRepository);
    @NotNull private final IUserService userService = new UserService(userRepository);
    @NotNull private final Scanner scanner = new Scanner(System.in);
    @NotNull private final Map<String, AbstractCommand> commands = new LinkedHashMap<>();
    private final Set<Class<? extends AbstractCommand>> classes =
            new Reflections("ru.habibrahmanov.tm.command").getSubTypesOf(AbstractCommand.class);

    public void init() throws Exception {
        registry(classes);
        start();
    }

    public void registry(@Nullable final Set<Class<? extends AbstractCommand>> classes) throws Exception {
            if (classes == null) return;
            for (Class clazz : classes) {
                registry(clazz);
            }
        userService.registryUser("log", "pas", "pas");
        userService.login("log", "pas");
        userService.getCurrentUser().setId("2222");
        projectService.persist("project_1", "It is my first project", "23.01.2020", "01.02.2020", "1111");
        projectService.persist("project_2", "It is my second project", "27.01.2020", "07.02.2020", "2222");
        projectService.persist("project_3", "It is my third project", "03.02.2020", "11.02.2020", "2222");
        projectService.persist("project_4", "It is my 4 project", "01.03.2020", "17.03.2020", "2222");
        projectService.persist("project_5", "It is my 5 project", "23.02.2020", "02.02.2020", "2222");
    }

    public void registry(@NotNull final Class clazz) throws IllegalAccessException, InstantiationException {
        @NotNull final AbstractCommand abstractCommand = (AbstractCommand) clazz.newInstance();
        abstractCommand.setBootstrap(this);
        @Nullable final String nameCommand = abstractCommand.getName();
        @Nullable final String descriptionCommand = abstractCommand.getDescription();
        if (nameCommand == null || nameCommand.isEmpty()) return;
        if (descriptionCommand == null || descriptionCommand.isEmpty()) return;
        commands.put(nameCommand, abstractCommand);
    }

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        System.out.println("Enter \"help\" to show all commands.");
        String command = "";
        while (!command.equals("exit")) {
            try {
                command = scanner.nextLine();
                execute(command);
            } catch (RemoveFailedException | IncorrectValueException | ListIsEmptyExeption e) {
                System.err.println(e.getMessage());
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if (command == null || command.isEmpty()) return;
        @Nullable final AbstractCommand abstractCommand = commands.get(command);
        if (abstractCommand == null) return;
        if (abstractCommand.secure() || (!abstractCommand.secure() && userService.isAuth())) {
            abstractCommand.execute();
        }
    }

    @Override
    public Map<String, AbstractCommand> getCommands() {
        return commands;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public Scanner getScanner() {
        return scanner;
    }

    @Override
    public IUserService getUserService() {
        return userService;
    }
}
