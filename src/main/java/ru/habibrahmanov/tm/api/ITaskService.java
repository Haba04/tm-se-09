package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;

public interface ITaskService {
    void persist(@Nullable String projectId, @Nullable String userId, @Nullable String name,
                 @Nullable String description, @Nullable String dateBegin, @Nullable String dateEnd) throws IncorrectValueException, ParseException;

    @Nullable Task findOne(@Nullable String taskId, @NotNull String userId) throws IncorrectValueException, ListIsEmptyExeption;

    @Nullable List<Task> findAll(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @Nullable List<Task> findAllSortedByDateBegin(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @Nullable List<Task> findAllSortedByDateEnd(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @Nullable List<Task> findAllSortedByStatus(@Nullable String userId) throws IncorrectValueException;

    @Nullable List<Task> searchByString (@Nullable String userId, @Nullable String string) throws IncorrectValueException, ListIsEmptyExeption;

    void remove(@Nullable String userId, @Nullable String taskId) throws IncorrectValueException, RemoveFailedException;

    void removeAll(@Nullable String userId, @Nullable String projectId) throws IncorrectValueException, RemoveFailedException;

    void update(@Nullable String userId, @Nullable String id, @Nullable String name, @Nullable String description,
                @Nullable String dateBegin, @Nullable String dateEnd) throws IncorrectValueException, ParseException;

    void merge(@Nullable String id, @Nullable String projectId, @Nullable String userId, @Nullable String name,
                    @Nullable String description, @Nullable String dateBegin, @Nullable String dateEnd) throws IncorrectValueException, ParseException;
}
