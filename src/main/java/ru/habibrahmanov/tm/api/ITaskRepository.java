package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Task;

import java.util.Date;
import java.util.List;

public interface ITaskRepository {
    void persist(@NotNull Task task);

    @Nullable Task findOne(@NotNull String taskId, @NotNull String userId);

    @Nullable List<Task> findAll(@NotNull String userId);

    @Nullable List<Task> findAllSortedByDateBegin(@NotNull String userId);

    @Nullable List<Task> findAllSortedByDateEnd(@NotNull String userId);

    @Nullable List<Task> findAllSortedByStatus(@NotNull String userId);

    @Nullable List<Task> searchByString (@NotNull String userId, @NotNull String string);

    boolean remove(@NotNull String userId, @NotNull String taskId);

    boolean removeAll(@NotNull String userId, @NotNull String projectId);

    boolean update(@NotNull String userId, @NotNull String id, @NotNull String name, @NotNull String description,
                @NotNull Date dateBegin, @NotNull Date dateEnd);

    void merge(@NotNull Task task);
}
