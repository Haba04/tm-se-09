package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;

public interface IProjectService {
    void persist(@Nullable String name, @Nullable String description, @Nullable String dateBegin,
                 @Nullable String dateEnd, @NotNull String userId) throws IncorrectValueException, ParseException;

    @Nullable List<Project> findAll(@NotNull String userId) throws IncorrectValueException, ListIsEmptyExeption;

    @Nullable List<Project> findAllSortedByDateBegin(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @Nullable List<Project> findAllSortedByDateEnd(@NotNull String userId) throws ListIsEmptyExeption, IncorrectValueException;

    @Nullable List<Project> findAllSortedByStatus(@Nullable String userId) throws IncorrectValueException;

    @Nullable List<Project> searchByString (@Nullable String projectId, @Nullable String string) throws IncorrectValueException, ListIsEmptyExeption;

    void removeAll(@Nullable String userId) throws IncorrectValueException, RemoveFailedException;

    void remove(@Nullable String projectId, @Nullable String userId) throws IncorrectValueException, RemoveFailedException;

    void update(@NotNull String userId, @Nullable String projectId, @Nullable String name, @Nullable String description,
                @Nullable String dateBegin, @Nullable String dateEnd) throws IncorrectValueException, ParseException;

    void merge(@Nullable String projectId, @Nullable String name, @Nullable String description,
               @Nullable String dateBegin, @Nullable String dateEnd, @Nullable String userId) throws IncorrectValueException, ParseException;

    @Nullable List<Project> findByAdding(@NotNull String userId);
}
