package ru.habibrahmanov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

public interface IUserService {
    void registryAdmin(
            @Nullable String login, @Nullable String password, @Nullable String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    void registryUser(
            @Nullable String login, @Nullable String password, @Nullable String passwordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    void updatePassword(
            @Nullable User currentUser, @Nullable String curPassword, @Nullable String newPassword,
            @Nullable String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException;

    User login(
            @Nullable String login, @Nullable String password
    ) throws IncorrectValueException, UnsupportedEncodingException, NoSuchAlgorithmException;

    User viewProfile();

    void editProfile(@Nullable String newLogin) throws IncorrectValueException;

    void logout();

    boolean isAuth();

    String createPasswordHashMD5(@NotNull String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, IncorrectValueException;

    User getCurrentUser();
}
