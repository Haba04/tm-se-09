package ru.habibrahmanov.tm.api;

import ru.habibrahmanov.tm.command.AbstractCommand;
import java.util.Map;
import java.util.Scanner;

public interface ServiceLocator {
    Map<String, AbstractCommand> getCommands();
    IProjectService getProjectService();
    ITaskService getTaskService();
    Scanner getScanner();
    IUserService getUserService();
}
