package ru.habibrahmanov.tm.comporator;

import ru.habibrahmanov.tm.entity.AbstractEntity;
import java.util.Comparator;

public final class DateEndComparator<T> implements Comparator<AbstractEntity> {
    @Override
    public int compare(AbstractEntity o1, AbstractEntity o2) {
        return o1.getDateEnd().compareTo(o2.getDateEnd());
    }
}