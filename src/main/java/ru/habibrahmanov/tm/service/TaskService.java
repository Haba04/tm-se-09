package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.ITaskRepository;
import ru.habibrahmanov.tm.api.ITaskService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Task;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;

public final class TaskService extends AbstractService implements ITaskService {
    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public void persist(
            @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.persist(
                new Task(UUID.randomUUID().toString(), projectId, userId, name,
                description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd))
        );
    }

    @Override
    public Task findOne(@Nullable final String taskId, @Nullable final String userId) throws IncorrectValueException, ListIsEmptyExeption {
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findOne(taskId, userId) == null) throw new ListIsEmptyExeption();
        return taskRepository.findOne(taskId, userId);
    }

    @Override
    public List<Task> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findAll(userId).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.findAll(userId);
    }

    @Override
    public List<Task> findAllSortedByDateBegin(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findAllSortedByDateBegin(userId).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.findAllSortedByDateBegin(userId);
    }

    @Override
    public List<Task> findAllSortedByDateEnd(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.findAllSortedByDateEnd(userId).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.findAllSortedByDateEnd(userId);
    }

    @Override
    public List<Task> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        return taskRepository.findAllSortedByStatus(userId);
    }

    @Override
    public List<Task> searchByString (
            @Nullable final String userId, @Nullable final String string
    ) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (taskRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        return taskRepository.searchByString(userId, string);
    }

    @Override
    public void remove(@Nullable final String userId, @Nullable final String taskId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (taskId == null || taskId.isEmpty()) throw new IncorrectValueException();
        if (!taskRepository.remove(userId, taskId)) throw new RemoveFailedException();
        taskRepository.remove(userId, taskId);
    }

    @Override
    public void removeAll(@Nullable final String userId, @Nullable final String projectId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (!taskRepository.removeAll(userId, projectId)) throw new RemoveFailedException();
        taskRepository.removeAll(userId, projectId);
    }

    @Override
    public void update(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description,
                       @Nullable final String dateBegin, @Nullable final String dateEnd) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.update(userId, id, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Override
    public void merge(
            @Nullable final String id, @Nullable final String projectId, @Nullable final String userId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (id == null || id.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        taskRepository.merge(new Task(id, projectId, userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }
}
