package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IProjectRepository;
import ru.habibrahmanov.tm.api.IProjectService;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.exeption.ListIsEmptyExeption;
import ru.habibrahmanov.tm.exeption.RemoveFailedException;

import java.text.ParseException;
import java.util.List;
import java.util.UUID;

public final class ProjectService extends AbstractService implements IProjectService {
    @NotNull private final IProjectRepository projectRepository;

    public ProjectService(@NotNull IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public void persist(
            @Nullable final String name, @Nullable final String description, @Nullable final String dateBegin,
            @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        projectRepository.persist(new Project(UUID.randomUUID().toString(), userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    @Override
    public List<Project> findAll(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.findAll(userId).isEmpty()) {
            throw new ListIsEmptyExeption();
        }
        return projectRepository.findAll(userId);
    }

    @Override
    public List<Project> findAllSortedByDateBegin(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.findAllSortedByDateBegin(userId).isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.findAllSortedByDateBegin(userId);
    }

    @Override
    public List<Project> findAllSortedByDateEnd(@Nullable final String userId) throws ListIsEmptyExeption, IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.findAllSortedByDateEnd(userId).isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.findAllSortedByDateEnd(userId);
    }

    @Override
    public List<Project> findAllSortedByStatus(@Nullable final String userId) throws IncorrectValueException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        return projectRepository.findAllSortedByStatus(userId);
    }

    @Override
    public List<Project> searchByString (@Nullable final String userId, @Nullable final String string) throws IncorrectValueException, ListIsEmptyExeption {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (string == null || string.isEmpty()) throw new IncorrectValueException();
        if (projectRepository.searchByString(userId, string).isEmpty()) throw new ListIsEmptyExeption();
        return projectRepository.searchByString(userId, string);
    }

    @Override
    public void removeAll(@Nullable final String userId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (!projectRepository.removeAll(userId)) throw new RemoveFailedException();
        projectRepository.removeAll(userId);
    }

    @Override
    public void remove(@Nullable final String projectId, @Nullable final String userId) throws IncorrectValueException, RemoveFailedException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (!projectRepository.remove(projectId, userId)) throw new RemoveFailedException();
        projectRepository.remove(projectId, userId);
    }

    @Override
    public void update(
            @Nullable final String userId, @Nullable final String projectId, @Nullable final String name,
            @Nullable final String description, @Nullable final String dateBegin, @Nullable final String dateEnd
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty()) throw new IncorrectValueException();
        if (description == null || description.isEmpty()) throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty()) throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty()) throw new IncorrectValueException();
        projectRepository.update(userId, projectId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd));
    }

    @Override
    public void merge(
            @Nullable final String projectId, @Nullable final String name, @Nullable final String description,
            @Nullable final String dateBegin, @Nullable final String dateEnd, @Nullable final String userId
    ) throws IncorrectValueException, ParseException {
        if (userId == null || userId.isEmpty()) throw new IncorrectValueException();
        if (projectId == null || projectId.isEmpty()) throw new IncorrectValueException();
        if (name == null || name.isEmpty())  throw new IncorrectValueException();
        if (description == null || description.isEmpty())  throw new IncorrectValueException();
        if (dateBegin == null || dateBegin.isEmpty())  throw new IncorrectValueException();
        if (dateEnd == null || dateEnd.isEmpty())  throw new IncorrectValueException();
        projectRepository.merge(new Project(projectId, userId, name, description, dateFormat.parse(dateBegin), dateFormat.parse(dateEnd)));
    }

    public List<Project> findByAdding(@NotNull final String userId) {
        return projectRepository.findAll(userId);
    }

    public IProjectRepository getProjectRepository() {
        return projectRepository;
    }
}
