package ru.habibrahmanov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.api.IUserRepository;
import ru.habibrahmanov.tm.api.IUserService;
import ru.habibrahmanov.tm.entity.User;
import ru.habibrahmanov.tm.enumeration.Role;
import ru.habibrahmanov.tm.exeption.IncorrectValueException;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

public final class UserService implements IUserService {
    @Nullable private final IUserRepository userRepository;
    @Nullable private User currentUser = null;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void registryAdmin(
            @Nullable final String login, @Nullable final String password, @Nullable final String passwordConfirm
    ) throws IllegalArgumentException, UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new IllegalArgumentException("SUCH USER EXISTS");
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        userRepository.persist(new User(login, createPasswordHashMD5(password), Role.ADMIN));
    }

    @Override
    public void registryUser(
            @Nullable final String login, @Nullable final String password, @Nullable final String passwordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException {
        if (login == null || login.isEmpty()) return;
        if (password == null || password.isEmpty()) return;
        if (userRepository.findByLogin(login) != null) throw new IllegalArgumentException("SUCH USER EXISTS");
        if (!password.equals(passwordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        userRepository.persist(new User(login, createPasswordHashMD5(password), Role.USER));
    }

    @Override
    public void updatePassword(
            @Nullable final User currentUser, @Nullable final String curPassword, @Nullable final String newPassword,
            @Nullable final String newPasswordConfirm
    ) throws UnsupportedEncodingException, NoSuchAlgorithmException, IncorrectValueException {
        if (currentUser == null) return;
        if (curPassword == null || curPassword.isEmpty()) return;
        if (newPassword == null || newPassword.isEmpty()) return;
        if (newPasswordConfirm == null || newPasswordConfirm.isEmpty()) return;
        if (!currentUser.getPassword().equals(createPasswordHashMD5(curPassword))) {
            throw new IllegalArgumentException("CURRENT PASSWORD DOES NOT MATCH USER PASSWORD: " + currentUser.getLogin());
        }
        if (!newPassword.equals(newPasswordConfirm)) throw new IllegalArgumentException("PASSWORDS DO NOT MATCH");
        currentUser.setPassword(createPasswordHashMD5(newPassword));
    }

    @Override
    public User login(
            @Nullable final String login, @Nullable final String password
    ) throws IncorrectValueException, UnsupportedEncodingException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new IncorrectValueException();
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        final List<User> userList = userRepository.findAll();
        if (userList == null || userList.isEmpty()) throw new IncorrectValueException("NO USER CREATED");
        if (userRepository.findByLogin(login) == null) throw new IncorrectValueException("USER WITH SUCH LOGIN DOES NOT EXIST");
        final String currentPassword = userRepository.findByLogin(login).getPassword();
        final String passwordMD5 = createPasswordHashMD5(password);
        if (!currentPassword.equals(passwordMD5)) throw new IncorrectValueException("WRONG PASSWORD");
        currentUser = userRepository.findByLogin(login);
        return currentUser;
    }

    @Override
    public User viewProfile() {
        return currentUser;
    }

    @Override
    public void editProfile(@Nullable final String newLogin) throws IncorrectValueException {
        if (newLogin == null || newLogin.isEmpty()) throw new IncorrectValueException();
        currentUser.setLogin(newLogin);
    }

    @Override
    public void logout() {
        currentUser = null;
    }

    @Override
    public boolean isAuth() {
        return currentUser != null;
    }

    @Override
    public String createPasswordHashMD5(@Nullable final String password) throws NoSuchAlgorithmException, UnsupportedEncodingException, IncorrectValueException {
        if (password == null || password.isEmpty()) throw new IncorrectValueException();
        @Nullable final byte[] bytePassword = password.getBytes("UTF-8");
        @Nullable final String md5Password = new String(MessageDigest.getInstance("MD5").digest(bytePassword));
        return md5Password;
    }

    @Override
    public User getCurrentUser() {
        return currentUser;
    }
}
