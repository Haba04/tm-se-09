package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;

public class TaskSearchByStringCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "task-search-string";
    }

    @Override
    public String getDescription() {
        return "search for tasks and tasks by part of title or description";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SEARCH FOR TASKS BY PART OF TITLE OR DESCRIPTION]");
        System.out.println("ENTER THE STRING");
        @Nullable final String string = serviceLocator.getScanner().nextLine();
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().searchByString(serviceLocator.getUserService().getCurrentUser().getId(), string);
        for (Task task : taskList) {
            System.out.println("STRING: \"" + string + "\"  FOUND IN PROJECTS WITH ID: " + task.getId());
        }
    }
}
