package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskUpdateCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-update";
    }

    @Override
    public String getDescription() {
        return "update task by id";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK UPDATE]");
        System.out.println("ENTER TASK ID:");
        @Nullable final String id = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK NAME:");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        @Nullable final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        serviceLocator.getTaskService().update(userId, id, name, description, dateBegin, dateEnd);
        System.out.println("TASK UPDATED SUCCESSFULLY");
    }
}
