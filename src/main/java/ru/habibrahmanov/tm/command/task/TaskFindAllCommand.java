package ru.habibrahmanov.tm.command.task;

import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskFindAllCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find-all";
    }

    @Override
    public String getDescription() {
        return "find all tasks";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SHOW ALL TASKS]");
        serviceLocator.getTaskService().findAll(serviceLocator.getUserService().getCurrentUser().getId()).forEach((v)-> System.out.println("TASK ID: " + v.getId() + " / NAME: " + v.getName()));
    }
}
