package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class TaskPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-persist";
    }

    @Override
    public String getDescription() {
        return "create new task";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        @Nullable final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println("ENTER TASK NAME");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DESCRIPTION");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE BEGIN");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER TASK DATE END");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        serviceLocator.getTaskService().persist(projectId, userId, name, description, dateBegin, dateEnd);
        System.out.println("TASK CREATE SUCCESSFULLY");
    }
}
