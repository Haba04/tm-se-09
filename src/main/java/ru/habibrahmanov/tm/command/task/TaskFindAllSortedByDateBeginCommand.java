package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.entity.Task;

import java.util.List;

public class TaskFindAllSortedByDateBeginCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-sort-date-begin";
    }

    @Override
    public String getDescription() {
        return "task find all sorted by date begin command";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL TASKS SORTED BY DATE BEGIN]");
        @NotNull final List<Task> taskList = serviceLocator.getTaskService().findAllSortedByDateBegin(serviceLocator.getUserService().getCurrentUser().getId());
        for (Task task : taskList) {
            System.out.println("TASK ID: " + task.getId() + " / NAME: " + task.getName());
            System.out.println("DESCRIPTION: " + task.getDescription());
            System.out.println("DATE BEGIN: " + dateFormat.format(task.getDateBegin()) + " / DATE END: " + dateFormat.format(task.getDateEnd()));
        }
    }
}
