package ru.habibrahmanov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public class TaskFindByAddingCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "task-find-adding";
    }

    @Override
    public String getDescription() {
        return "shows all tasks in the order they were added";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK FIND BY ADDING]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findByAdding(serviceLocator.getUserService().getCurrentUser().getId());
        projectList.forEach((v) -> System.out.println("TASK ID: " + v.getId() + ", TASK NAME: " + v.getName()));

    }
}
