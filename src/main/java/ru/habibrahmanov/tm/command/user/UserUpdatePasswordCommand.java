package ru.habibrahmanov.tm.command.user;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class UserUpdatePasswordCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "password-update";
    }

    @Override
    public String getDescription() {
        return "user password update";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[UPDATE PASSWORD]");
        System.out.println("ENTER CURRENT PASSWORD:");
        @Nullable final String currentPassword = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER NEW PASSWORD:");
        @Nullable final String newPassword = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER NEW PASSWORD AGAIN:");
        @Nullable final String newPasswordConfirm = serviceLocator.getScanner().nextLine();
        serviceLocator.getUserService().updatePassword(serviceLocator.getUserService().getCurrentUser(), currentPassword, newPassword, newPasswordConfirm);
        System.out.println("UPDATE PASSWORD SUCCESSFULLY");
    }

}
