package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public class ProjectFindAllSortedByStatusCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-sort-status";
    }

    @Override
    public String getDescription() {
        return "project sort by status";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS SORTED BY DATE END]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findAllSortedByStatus(serviceLocator.getUserService().getCurrentUser().getId());
        for (Project project : projectList) {
            System.out.println("PROJECT ID: " + project.getId() + ", NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("DATE BEGIN: " + dateFormat.format(project.getDateBegin()) + ", DATE END: " + dateFormat.format(project.getDateEnd()));
            System.out.println("STATUS: " + project.getStatus());
            System.out.println();
        }
    }
}
