package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public class ProjectSearchByStringCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-search-string";
    }

    @Override
    public String getDescription() {
        return "search for projects and tasks by part of title or description";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[SEARCH FOR PROJECTS BY PART OF TITLE OR DESCRIPTION]");
        System.out.println("ENTER THE STRING");
        @Nullable final String string = serviceLocator.getScanner().nextLine();
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().searchByString(serviceLocator.getUserService().getCurrentUser().getId(), string);
        for (Project project : projectList) {
            System.out.println("STRING: \"" + string + "\"  FOUND IN PROJECTS WITH ID: " + project.getId());
        }

    }
}
