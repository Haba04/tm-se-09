package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.entity.Project;
import ru.habibrahmanov.tm.command.AbstractCommand;

import java.util.List;
import java.util.Map;

public final class ProjectFindAllCommand extends AbstractCommand {

    @Override
    public String getName() {
        return "project-find-all";
    }

    @Override
    public String getDescription() {
        return "show all projects";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS]");
        @Nullable final List<Project> projectList = serviceLocator.getProjectService().findAll(serviceLocator.getUserService().getCurrentUser().getId());
        for (Project project : projectList) {
            System.out.println("PROJECT ID: " + project.getId() + " / NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("DATE BEGIN: " + dateFormat.format(project.getDateBegin())  + " / DATE END: " + dateFormat.format(project.getDateEnd()));
            System.out.println("STATUS: " + project.getStatus());
            System.out.println();
        }
    }
}