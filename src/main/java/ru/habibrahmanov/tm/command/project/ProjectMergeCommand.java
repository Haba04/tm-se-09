package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectMergeCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-merge";
    }

    @Override
    public String getDescription() {
        return "update if project is already exist, else create new project";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT MERGE]");
        System.out.println("ENTER PROJECT ID");
        @Nullable final String projectId = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER PROJECT NAME");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        @Nullable final String userId = serviceLocator.getUserService().getCurrentUser().getId();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().merge(projectId, userId, name, description, dateBegin, dateEnd);
        System.out.println("PROJECT MERGE SUCCESSFULLY");
    }
}
