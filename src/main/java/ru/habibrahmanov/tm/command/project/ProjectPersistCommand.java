package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.Nullable;
import ru.habibrahmanov.tm.command.AbstractCommand;

public final class ProjectPersistCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-persist";
    }

    @Override
    public String getDescription() {
        return "create new project.";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        @Nullable final String name = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DESCRIPTION:");
        @Nullable final String description = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE BEGIN:");
        @Nullable final String dateBegin = serviceLocator.getScanner().nextLine();
        System.out.println("ENTER DATE END:");
        @Nullable final String dateEnd = serviceLocator.getScanner().nextLine();
        serviceLocator.getProjectService().persist(name, description, dateBegin, dateEnd, serviceLocator.getUserService().getCurrentUser().getId());
        System.out.println("CREATE NEW PROJECT SUCCESSFULLY");
    }
}
