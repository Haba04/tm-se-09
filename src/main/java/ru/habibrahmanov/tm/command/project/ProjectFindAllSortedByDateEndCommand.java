package ru.habibrahmanov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.habibrahmanov.tm.command.AbstractCommand;
import ru.habibrahmanov.tm.entity.Project;

import java.util.List;

public class ProjectFindAllSortedByDateEndCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "project-sort-date-end";
    }

    @Override
    public String getDescription() {
        return "project find all sorted by date end command";
    }

    @Override
    public boolean secure() {
        return false;
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[FIND ALL PROJECTS SORTED BY DATE END]");
        @NotNull final List<Project> projectList = serviceLocator.getProjectService().findAllSortedByDateEnd(serviceLocator.getUserService().getCurrentUser().getId());
        for (Project project : projectList) {
            System.out.println("PROJECT ID: " + project.getId() + ", NAME: " + project.getName());
            System.out.println("DESCRIPTION: " + project.getDescription());
            System.out.println("DATE END: " + dateFormat.format(project.getDateBegin()) + ", DATE END: " + dateFormat.format(project.getDateEnd()));
            System.out.println("STATUS: " + project.getStatus());
            System.out.println();
        }
    }
}
