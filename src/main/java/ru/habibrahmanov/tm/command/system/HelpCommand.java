package ru.habibrahmanov.tm.command.system;

import ru.habibrahmanov.tm.command.AbstractCommand;

public class HelpCommand extends AbstractCommand {
    @Override
    public String getName() {
        return "help";
    }

    @Override
    public String getDescription() {
        return "Show all commands.";
    }

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public void execute() throws Exception {
        for (final AbstractCommand command : serviceLocator.getCommands().values()) {
            System.out.printf("%-25s > %s \n", command.getName(), command.getDescription());
        }
    }
}
